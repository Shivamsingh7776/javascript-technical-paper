# String methods

## string.length():
it will return the length of string.
### Example:-
```JavaScript
let text = "Shivam"
console.log(text.length);
```
### output:-
```
6
```
### String slice():
* slice() extracts a part of a string and returns the extracted part in a new string .
* The method takes 2 parameters: start position, and end position (end not included).

### Example:-
```javascript
let text = "Apple, Banana, Kiwi";
let part = text.slice(7, 13);
console.log(part);
```

### output:-

```
Banana
```

### example:-
```javascript
let text = "Apple, Banana, Kiwi";
let part = text.slice(7);
console.log(part);
```

### output:-
```
Banana, Kiwi
```
### Example:-
```javascript
let text = "Apple, Banana, Kiwi";
let part = text.slice(-12); //indexing start from end
console.log(part);
```

### output:-
```
Banana, Kiwi
```
## JavaScript String substring():
substring() is similar to slice().

The difference is that start and end values less than 0 are treated as 0 in substring().

### Example:-
```javascript
let str = "Apple, Banana, Kiwi";
let part1 = str.substring(7, 13);
let part2 = str.substr(7, 6);
let part3 = str.substr(-4);
console.log(part1);
console.log(part2);
console.log(part3);
```
### output:
```
Banana
Banana
Kiwi
```
## Replacing String Content:
The replace() method replaces a specified value with another value in a string:

### Example:-
```javascript
let text = "Please visit Microsoft!";
let newText = text.replace("Microsoft", "google");
console.log(newText);
```

### output:-
```
Please visit google!
```
* By default, the replace() method is case sensitive. Writing MICROSOFT (with upper-case) will not work:

### Example:-
```javascript
let text = "Please visit Microsoft!";
let newText = text.replace("MICROSOFT", "W3Schools");
console.log(newText);
```
### output:-
```
Please visit Microsoft!
```

### Example for converting upper case to lower
```javascript
let text = "Please visit Microsoft!";
let newText = text.replace(/MICROSOFT/i, "google");
console.log(newText);
```

### output:-
```
Please visit google!
```
## JavaScript String ReplaceAll()
it replace all the matching things

### Example:-
```javascript
let text = "I love cats. Cats are very easy to love. Cats are very popular."
text = text.replaceAll("Cats","Dogs");
text = text.replaceAll("cats","dogs");

console.log(text);
```

### output:-
```
I love dogs. Dogs are very easy to love. Dogs are very popular.
```

## Converting to Upper and Lower Case:
* A string is converted to upper case with toUpperCase():
* A string is converted to lower case with toLowerCase():
### Example:-
```javascript
let text1 = "Hello World!";
let text2 = text1.toUpperCase();
console.log(text2);
```
### output:-
```
HELLO WORLD!
```
## JavaScript String concat()
concat() joins two or more strings:

### Example:-
```javascript
let text1 = "Hello";
let text2 = "World";
let text3 = text1.concat(" ", text2);
console.log(text3);
```

### output:-
```
Hello World
```
## JavaScript String trim():
The trim() method removes whitespace from both sides of a string:

### Example:-
```javascript
let text1 = "      Hello World!      ";
let text2 = text1.trim();
console.log(text2.length);
```

### output:-
```
12
```
## JavaScript String trimStart()
The trimStart() method works like trim(), but removes whitespace only from the start of a string.

## JavaScript String trimEnd()
The trimEnd() method works like trim(), but removes whitespace only from the end of a string.

## JavaScript String Padding
### JavaScript String padStart()
The padStart() method pads a string with another string:

### Example:-
```javascript
let text = "5";
let padded = text.padStart(4,"x");
console.log(padded);
```
### output:-
```
xxx5
```
### JavaScript String padEnd():
The padEnd() method pads a string with another string:

### Example:-
```javascript
let text = "5";
let padded = text.padEnd(4,"x");
console.log(padded);

```
### output:-
```
5xxx
```