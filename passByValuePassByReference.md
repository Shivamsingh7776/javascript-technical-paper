## Pass by Reference:
In Pass by Reference, Function is called by directly passing the reference/address of the variable as an argument. So changing the value inside the function also change the original value. In JavaScript array and Object follows pass by reference property.

In Pass by reference, parameters passed as an arguments does not create its own copy, it refers to the original value so changes made inside function affect the original value. 

### Example:-
```javascript
    function passByReference(object){
        let temp = object.a;
        object.a = object.b;
        object.b = temp;

        console.log(`Inside Pass By Reference Function -> a = ${object.a} b = ${object.b}`);
    }

    let object = {
        a : 10,
        b : 20
    }

    console.log(`Before calling pass by referance  Function a -> a = ${object.a} b = ${object.b}`);


    passByReference(object)
     
    console.log(`After calling Pass By Reference
        Function -> a = ${object.a} b = ${object.b}`);

```

### output:-
```
Before calling pass by referance  Function a -> a = 10 b = 20
Inside Pass By Reference Function -> a = 20 b = 10
After calling Pass By Reference
    Function -> a = 20 b = 10
```

## Pass by Value:-
In Pass by value, function is called by directly passing the value of the variable as an argument. So any changes made inside the function does not affect the original value.

In Pass by value, parameters passed as an arguments create its own copy. So any changes made inside the function is made to the copied value not to the original value .

### Example:-

```javascript
function Passbyvalue(a, b) {
    let temp;
    temp = b;
    b = a;
    a = temp;
    console.log(`Inside Pass by value function -> a = ${a} b = ${b}`);
}
 
let a = 1;
let b = 2;
console.log(`Before calling Pass by value Function -> a = ${a} b = ${b}`);
 
Passbyvalue(a, b);
 
console.log(`After calling Pass by value Function -> a =${a} b = ${b}`);
```

### output:-

```
Before calling Pass by value Function -> a = 1 b = 2
Inside Pass by value function -> a = 2 b = 1
After calling Pass by value Function -> a =1 b = 2
```
