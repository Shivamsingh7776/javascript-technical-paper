# JavaScript Debugging
## Code Debugging
* Programming code might contain syntax errors, or logical errors.
* Often, when programming code contains errors, nothing will happen. There are no error messages, and you will get no indications where to search for errors.
* Many of these errors are difficult to diagnose.
* Searching for (and fixing) errors in programming code is called code debugging.

## Setting Breakpoints
* In the debugger window, you can set breakpoints in the JavaScript code.
* At each breakpoint, JavaScript will stop executing, and let you examine JavaScript values.
* After examining values, you can resume the execution of code (typically with a play button).

## The console.log() Method
If your browser supports debugging, you can use console.log() to display JavaScript values in the debugger window:

### Example:
```javascript
 <!DOCTYPE html>
<html>
<body>

<h1>My First Web Page</h1>

<script>
a = 5;
b = 6;
c = a + b;
console.log(c);
</script>

</body>
</html> 
```
