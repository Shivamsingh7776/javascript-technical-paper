# JavaScript Scope
* Scope determines the accessibility (visibility) of variables.
JavaScript has three types of scope:
1. Block scope
2. Function scope
3. Global scope

## Bloack Scope:
Variables declared inside a { } block cannot be accessed from outside the block:

### Example:-
```javascript
{
  let x = 2;
}
// x can NOT be used here 
```
* Variables declared with the var keyword can NOT have block scope.
* Variables declared inside a { } block can be accessed from outside the block. 
### Example:-
```javascript
{
  var x = 2;
}
// x CAN be used here 
```

## Local Scope:
Variables declared within a JavaScript function, become LOCAL to the function.

### Example:
```javascript
// code here can NOT use carName

function myFunction() {
  let carName = "Volvo";
  // code here CAN use carName
}

// code here can NOT use carName
```

## Function Scope:
* JavaScript has function scope: Each function creates a new scope.
* Variables defined inside a function are not accessible (visible) from outside the function.
* Variables declared with var, let and const are quite similar when declared inside a function.

They all have Function Scope:

```javascript
function myFunction() {
  var carName = "Volvo";   // Function Scope
}
```

```javascript
function myFunction() {
  let carName = "Volvo";   // Function Scope
}
```

```javascript
function myFunction() {
  const carName = "Volvo";   // Function Scope
}
```

### Global JavaScript Variables:
A variable declared outside a function, becomes GLOBAL.

### Example:-
```javascript
let carName = "Volvo";
// code here can use carName

function myFunction() {
// code here can also use carName
}
```
### Global Scope:
* Variables declared Globally (outside any function) have Global Scope.
* Global variables can be accessed from anywhere in a JavaScript program.
* Variables declared with var, let and const are quite similar when declared outside a block.

They all have Global Scope:

```javascript
var x = 2;       // Global scope
```
```javascript
let x = 2;       // Global scope 
```
```javascript
const x = 2;       // Global scope 
```