# Loops

## for 
### Syntax of for loop

```javaScript
{
    for(let expression1; expression2; expression3)
    // Code block to be executed according to condition
}

```

### Example:-
``` javascript
{
   for(let index = 0; index <= 10; index++){
      console.log('The number is' + index);
   }
}
```
### output-
```
    The number is 0
    The number is 1
    The number is 2
    The number is 3
    The number is 4
    The number is 5
    The number is 6
    The number is 7
    The number is 8
    The number is 9
    The number is 10
```

## forEach
The forEach() method executes a provided function once for each array element. 

### Example:-
```javascript
    const person = [
        {firstName : 'Shivam'}, 
        {lastName : 'Singh'},
        {
            address : {
                street : '50 main st',
                city : 'Boston',
                state : 'MA'
            }
        }
    ]
    person.forEach(element => console.log(element));

```

### output:-

```
{ firstName: 'Shivam' }
{ lastName: 'Singh' }
{ address: { street: '50 main st', city: 'Boston', state: 'MA' } }
```
* it will print the all array element one by one in next line.

## The For In Loop:
The JavaScript for in statement loops through the properties of an Object:

### Syntax
``` javaScript
    for (key in object) {
  // code block to be executed
  }
```
### Example:-
```javascript
    const person = {
        firstName : 'Shivam',
        lastName : 'Singh',
        age : 20,
        hobbies : ['Cricket', 'Movie', 'Music'],
        address : {
            street : '50 main st',
            city : 'Boston',
            state : 'MA'
        }

    }

    for(key in person){
        console.log(person[key]);
    }

```
### output:-
```
    Shivam
    Singh
    20
    [ 'Cricket', 'Movie', 'Music' ]
    { street: '50 main st', city: 'Boston', state: 'MA' }
```

### Example Explain:-
* The for in loop iterates over a person object.
* Each itration returns a keys(key).
* The key is used to access the value of the key.
* The value of the key in person[key]

## The For Of Loop:-
he JavaScript for of statement loops through the values of an iterable object.

### Syntax:-
```javascript
    for(variable of iterable){
        //code block to be executed
    }

```

### Example:-

```javascript
    const person = [
        {firstName : 'Shivam'}, 
        {lastName : 'Singh'},
        {
            address : {
                street : '50 main st',
                city : 'Boston',
                state : 'MA'
            }
        }
    ]

    for(let key of person){
        console.log(key);
    }

```

### output:-

```
    { firstName: 'Shivam' }
    { lastName: 'Singh' }
    { address: { street: '50 main st', city: 'Boston', state: 'MA' } }
```

* It will iterate over whole object inside the array not only key .

## while
The while loop loops through a block of code as long as a specified condition is true.
### Syntax
```javascript
   while(consdition){
    //block to be executed
   }
```

### Example:
```javascript
    let index = 0;

    while(index <= 10){
        console.log("Number is a " + index);
        index += 1;
    }

```

### output

```
    Number is a 0
    Number is a 1
    Number is a 2
    Number is a 3
    Number is a 4
    Number is a 5
    Number is a 6
    Number is a 7
    Number is a 8
    Number is a 9
    Number is a 10

```
