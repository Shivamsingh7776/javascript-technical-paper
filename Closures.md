# JavaScript Closures
* JavaScript variables can belong to the local or global scope.
* Global variables can be made local (private) with closures.

## Global Variables
A function can access all variables defined inside the function, like this:
### Example:-
```javascript
function myFunction() {
  let a = 4;
  return a * a;
}
```

But a function can also access variables defined outside the function, like this:

### Example:-
```javascript
let a = 4;
function myFunction() {
  return a * a;
}
```

## Note:
Variables created without a declaration keyword (var, let, or const) are always global, even if they are created inside a function.

### Example:-
```javascript
function myFunction() {
  a = 4;
}
```
## A Counter Dilemma
* Suppose you want to use a variable for counting something, and you want this counter to be available to all functions.
* we could use a global variable, and a function to increase the counter:

### Example:-
```javascript
// Initiate counter
let counter = 0;

// Function to increment counter
function add() {
  counter += 1;
}

// Call add() 3 times
add();
add();
add();

// The counter should now be 3
```