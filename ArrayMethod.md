## Basics:-
### JavaScript Arrays:-
An array is a special variable, which can hold more than one value:
```javascript
const names = ['Shivam', 'Zaid', 'Krishna', 'Dikshant'];
```
### Creating an Array:-
### Syntax:-
```javascript
   const array_name = [item1, item2, item3, .....];
```
### Example:-
```javascript
   const fruits = ['Apple', 'Orange', 'Banana'];
   console.log(fruits);
```

### output:-
```
[ 'Apple', 'Orange', 'Banana' ]
```
### we can also create an array, and then provide the elements:
### Example:-
```javascript
   const fruits = []
   fruits[0] = 'Apple';
   fruits[1] = 'Orange';
   fruits[2] = 'Banana';
   console.log(fruits);

```
### output:-
```
[ 'Apple', 'Orange', 'Banana' ]
```

## Array methods:-
### Array.pop:- 
pop() method removes the last element from an array:

### Example:-
```javascript
const name = ['Shivam', 'Harsh', 'Zaid', 'Krishna', 'Ritesh'];
name.pop();
console.log(name);
```
### output:-
```
[ 'Shivam', 'Harsh', 'Zaid', 'Krishna' ]
```
### Array.push:-
push() method adds a new element to an array at the end.

### Example-
```javascript
const name = ['Shivam', 'Harsh', 'Zaid', 'Krishna', 'Ritesh'];
name.push('Manish');
console.log(name);
```
### output:-
```
['Shivam', 'Harsh', 'Zaid', 'Krishna', 'Ritesh','Manish']
```

### Array.concat
concat() method creates a new array by merging existing arrays:
### Example:-
```javascript
const nameList1 = ['Shivam','Zaid'];
const nameList2 = ['Harsh', 'Ritesh'];

const totalList = nameList1.concat(myList2);

console.log(totalList);
```
### output :-
```
['Shivam', 'Zaid', 'Harsh', 'Ritesh' ]
```
### Array.slice:-
* slice() method slices out a piece of an array.

slice() method slices out a piece of an array into a new array.

### Example:-
```javascript 
const fruits = ['Banana', 'Orange', 'Lemon', 'Apple', 'Mango'];
const citrus = fruits.slice(1);
```

### output:-
```
[ 'Orange', 'Lemon', 'Apple', 'Mango' ]
```
* The slice() method can take two arguments like slice(1, 3).
The method then selects elements from the start argument, and up to (but not including) the end argument.

### Example:-

```javascript
const fruits = ['Banana', 'Orange', 'Lemon', 'Apple', 'Mango'];
const citrus = fruits.slice(1,3);
console.log(citrus);

```
### output:-
```
[ 'Orange', 'Lemon' ]
```

### Array.splice:-
splice() method can be used to add new items to an array:
### Example:-
```javascript
const fruits = ['banana', 'Orange', 'Apple', 'Mango'];
fruits.splice(2, 0, 'Leamon', 'Kiwi');
console.log(fruits);
```
### output:-
```
[ 'banana', 'Orange', 'Leamon', 'Kiwi', 'Apple', 'Mango' ]
```
* The first parameter(2) defines the position where new elements should be added 
* The second (0) defines how many elements should be removed.
* The rest of the parameters('Lemon', 'Kiwi') defines the new elements to be added .

The splice() method returns an array with the deleted items:

### Example:-
```javascript
const fruits = ["Banana", "Orange", "Apple", "Mango"];
let removed = fruits.splice(2, 2, "Lemon", "Kiwi");
console.log(fruits);
console.log(removed);
```
### output:-
```
[ 'Banana', 'Orange', 'Lemon', 'Kiwi' ]
[ 'Apple', 'Mango' ]
```
### Array.join:-
* The join() method returns array as a string.
* The join() method does not change the original array.

### Syntax:-
```javascript
array.join(separator);
```

### Example:-
```javascript
const name = ['Shivam', 'Harsh', 'Zaid', 'Krishna', 'Ritesh'];
let text = name.join("-");
console.log(text);
```

### output:-
```
Shivam-Harsh-Zaid-Krishna-Ritesh
```

### Array.flat:-
The flat() method creates a new array with all sub-array elements concatenated into it recursively up to the specified depth. 

### Example-

```javascript
const arr1 = [0, 1, 2, [3, 4]];

console.log(arr1.flat());
// expected output: [0, 1, 2, 3, 4]

const arr2 = [0, 1, 2, [[[3, 4]]]];

console.log(arr2.flat(2));
// expected output: [0, 1, 2, [3, 4]]
```
### output:-
```
[ 0, 1, 2, 3, 4 ]
[ 0, 1, 2, [ 3, 4 ] ]
```

## Finding:
* The find() method return first element that we are searching .
* The find method executes a function for each array element.
* The find() method return undefined if no element are found.
* The find() method does not execute the function for empty elements.
* The find method does not change the original array.

### Syntax:-
```javascript
array.find(function(currentValue, index, arr), thisValue);
```
### Example:-
```javascript
const array = [1,2,3,4,5,6];

const found = array.find(element => element > 3);

console.log(found);
```
### output:-
```
4
```

## Array.indexOf:-
The indexOf() method returns the first index at which a given element can be found in the array, or -1 if it is not present. 

### Example:-
```javascript
const array = [1, 5, 6, 3, 2, 5];
console.log(array.indexOf(5));
console.log(array.indexOf(5,2));
console.log(array.indexOf(8));
```
### output:-

```
1
5
-1
```
## Array.includes:-
The includes() method determines whether an array includes a certain value among its entries, returning true or false as appropriate. 

### Example:-

```javascript
const array = [1, 2, 3, 4];
console.log(array.includes(2));
console.log(array.includes(5));
```
### output:-
```
true
false
```

## Array.findIndex:-
The findIndex() method returns the index of the first element in an array that satisfies the provided testing function. If no elements satisfy the testing function, -1 is returned. 

### Example:-
```javascript
const array = [5, 8, 10, 20, 6];
const indexIs1 = (element) => element > 10;
const indexIs2 = (element) => element > 20;
console.log(array.findIndex(indexIs1));
console.log(array.findIndex(indexIs2));
```

### output:-
```
3
-1
```
## Higher Order Functions:

### Array.forEach immutable
The forEach() method executes a provided function once for each array element.

### Example:-

```javascript
const array = [1,4,6,8,10]

array.forEach(element => console.log(element));
```

### output:-

```
1
4
6
8
10
```
### Array.filter
The filter() method creates a shallow copy of a portion of a given array, filtered down to just the elements from the given array that pass the test implemented by the provided function.

### Example:-
```javascript
const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

const result = words.filter(word => word.length > 6);

console.log(result);
```
### output:-
```
["exuberant", "destruction", "present"]
```

### Example:-
```javascript
const users = [
    {firstName: "Shivam", lastName: "Singh", age: 20},
    {firstName: "Zaid", lastName: "Khan", age: 19},
];

const output = users.filter((elements) => elements.age < 20)

console.log(output);
```
### output:-
```
[ { firstName: 'Zaid', lastName: 'Khan', age: 19 } ]
```
## Array.map:-
The map() method creates a new array populated with the results of calling a provided function on every element in the calling array. 

### Example:-
```javascript
const number = [3, 4, 6, 23, 25];
const newNumber = number.map(Math.sqrt);

console.log(newNumber);
```

### output:-
```
[1.7320508075688772, 2, 2.449489742783178, 4.795831523312719, 5 ]
```

### Example:-
```javascript
const array = [1, 4, 6, 19];

const newArray = array.map(elements => elements * elements);

console.log(newArray);
```

### output:-

```
[ 1, 16, 36, 361 ]
```

## Array.reduce:-
 The reduce() method executes a user-supplied "reducer" callback function on each element of the array, in order, passing in the return value from the calculation on the preceding element. The final result of running the reducer across all elements of the array is a single value.

The first time that the callback is run there is no "return value of the previous calculation". If supplied, an initial value may be used in its place. Otherwise the array element at index 0 is used as the initial value and iteration starts from the next element (index 1 instead of index 0). 

### Example:-
```javascript
    const Number = [1, 4 ,10, 8, 20, 23];

    const output = Number.reduce(function(acc, max){
        if(acc > max){
            max = acc;
        }
        return max;
    });

    console.log(output);
```
### Output:-
```
23
```
## Array.sort:-
The sort() method sorts an array alphabetically:

### Example:-
```javascript
const fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.sort());
```
### output:-
```
[ 'Apple', 'Banana', 'Mango', 'Orange' ]
```
### Example:-
```javascript
const number = [2, 1, 5, 4, 10, 20]
console.log(number.sort());

```
### output:-
```javascript
//Its sort the number according to the first digit number
[ 1, 10, 2, 20, 4, 5 ]
```
## Advanced methods-
### Array methods chaining:-

### Example:-
```javascript
const users = [
    {firstName: "Shivam", lastName: "Singh", age: 20},
    {firstName: "Zaid", lastName: "Khan", age: 19},
    {firstName:"Dikshant", lastName: "Sharma", age:20},
    {firstName:"Manish", lastName: "singh", age:25}
];

const output = users.filter((elemets) => elemets.age >= 20).map((element) => element.firstName);

console.log(output);
```
### output:-
```
[ 'Shivam', 'Dikshant', 'Manish' ]
```
### Example:-
```javascript
const users = [
    {firstName: "Shivam", lastName: "Singh", age: 20},
    {firstName: "Zaid", lastName: "Khan", age: 19},
    {firstName:"Dikshant", lastName: "Sharma", age:20},
    {firstName:"Manish", lastName: "singh", age:25}
];

const output = users.reduce(function(acc, curr){
    if(acc[curr.age]){
        acc[curr.age] += acc[curr.age];
    }else{
        acc[curr.age] = 1;
    }
    return acc;
}, {});


console.log(output)
```
### output:-

```
{ '19': 1, '20': 2, '25': 1 }
```








