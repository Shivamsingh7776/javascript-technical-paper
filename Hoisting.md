# JavaScript Hoisting

## Hoisting:-
is a concept that enables us to extract values of variables and functions even before initializing/assigning value without getting errors and this happens during the 1st phase (memory creation phase) of the Execution Context.

## Features of Hoisting:
* In JavaScript, Hoisting is the default behavior of moving all the declarations at the top of the scope before code execution. Basically, it gives us an advantage that no matter where functions and variables are declared, they are
* It allows us to call functions before even writing them in our code. 

### Sequence of variable declaration:
```
Declaration –> Initialisation/Assignment –> Usage 
```

### Variable lifecycle:
```javascript
let a;                  // Declaration
a = 100;            // Assignment
console.log(a);  // Usage
```

### Example:-
```javascript

	// Hoisting
	function codeHoist(){
		a = 10;
		let b = 50;
	}
	codeHoist();

	console.log(a); // 10
	console.log(b); // ReferenceError : b is not defined
```

### Explanation:
 In the above code, we created a function called codeHoist() and in there we have a variable that we didn’t declare using let/var/const and a let variable b. The undeclared variable is assigned the global scope by javascript hence we are able to print it outside the function, but in case of the variable b the scope is confined and it is not available outside and we get a ReferenceError.