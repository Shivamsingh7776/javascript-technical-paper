# Mutable and Immutable Methods (in strings and arrays)

## Mutable:
A mutable value is one that can be changed without creating an entirely new value. In JavaScript, objects and arrays are mutable by default, but primitive values are not — once a primitive value is created, it cannot be changed, although the variable that holds it may be reassigned.

### Example:-
```javaScript
    const Name = ['shivam', 'zaid', 'dikshant', 'ritesh']

    Name.push('Krishna');

    newName = Name;

    console.log(newName);

```
### output:-
```
[ 'shivam', 'zaid', 'dikshant', 'ritesh', 'Krishna' ]
```

### Example:-
```javaScript
    const str = 'javascript';

    str[2] = 'e';

    console.log(str);

```

### output:
```
javascript
```

* Previous example does not through any kind of error but the value at third index also uncahnge.

## Immutable:-
An immutable value is one whose content cannot be changed without creating an entirely new value.

In JavaScript, primitive values are immutable — once a primitive value is created, it cannot be changed, although the variable that holds it may be reassigned another value. By contrast, objects and arrays are mutable by default 
* their properties and elements can be changed without reassigning a new value. 

## Example:-
``` javaScript
    const number = 12;

    number = 13;

    console.log(number);

```
## output:
```
  TypeError: Assignment to constant variable.

```