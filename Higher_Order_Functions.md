# Higher Order Functions
The usual way you think about JavaScript functions is as reusable pieces of code that make some calculations. The arguments are the function's input data, and the return value is the output.

Here's a simple function that sums an array of numbers:

## Example:-
```javascript
function calculate(numbers) {
  let sum = 0;
  for (const number of numbers) {
    sum = sum + number;
  }
  return sum;
}
calculate([1, 2, 4]); // => 7
```

In the previous example, calculate([1, 2, 4]) accepts an array of numbers as an argument, and returns the number 7 — the sum.

But is it possible to use functions as values? Assign functions themselves to variables, use them as arguments, or even return? Yes, that's possible!

All because functions in JavaScript are first-class citizens. This means that you can:

### A. Assign functions to variables:
```javascript
// Assign to variables
const hiFunction = function() { 
  return 'Hello!' 
};
hiFunction(); // => 'Hello!'
```
### B. Use functions as arguments to other functions:
```javascript
// Use as arguments
function iUseFunction(func) {
  return func();
}
iUseFunction(function () { return 42 }); // => 42
```
### C. And even return functions from functions:
```javascript
// Return function from function
function iReturnFunction() {
  return function() { return 42 };
}
const myFunc = iReturnFunction();
myFunc(); // => 42
```

##  The higher-order functions in practice

### Example:-
```javascript
function calculate(operation, initialValue, numbers) {
  let total = initialValue;
  for (const number of numbers) {
    total = operation(total, number);
  }
  return total;
}
function sum(n1, n2) {
  return n1 + n2;
}
function multiply(n1, n2) {
  return n1 * n2;
}
calculate(sum, 0, [1, 2, 4]);      // => 7
calculate(multiply, 1, [1, 2, 4]); // => 8
```

## Examples of higher-order functions
```javascript
const numbers = [1, 2, 4];
const doubles = numbers.map(function mapper(number) {
  return 2 * number;
});
doubles; // [2, 4, 8]
```
